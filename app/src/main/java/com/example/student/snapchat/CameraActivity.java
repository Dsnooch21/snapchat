package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        CameraFragment cameraMain = new CameraFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.camera, cameraMain).commit();
    }

}
