package com.example.student.snapchat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class FriendsList extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);

        FriendlistFragment friendMenu = new FriendlistFragment();
        //AddFriendFragment addMenu = new AddFriendFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.friends, friendMenu).commit();
    }

}
