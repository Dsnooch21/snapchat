package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);

       ScreenFragment screenMenu = new ScreenFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.screen, screenMenu).commit();
    }
}
