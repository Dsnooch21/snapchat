package com.example.student.snapchat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;


public class MainMenuFragment extends Fragment {

    public MainMenuFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R .layout.fragment_main_menu, container, false);
        // Inflate the layout for this fragment


        Button registerButton = (Button) view.findViewById(R.id.register);
        Button loginButton = (Button) view.findViewById(R.id.login);
        Button mainButton = (Button) view.findViewById(R.id.screen);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                Toast.makeText(getActivity(), "wwop", Toast.LENGTH_SHORT).show();

            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivity(intent);
                Toast.makeText(getActivity(), "yay", Toast.LENGTH_SHORT).show();

            }
        });
        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Screen.class);
                startActivity(intent);
                Toast.makeText(getActivity(), "afsdfasdff", Toast.LENGTH_SHORT).show();

            }
        });
        return view;
    }

}
